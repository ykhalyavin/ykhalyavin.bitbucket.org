$.ajaxLoad = true;

//required when $.ajaxLoad = true
$.defaultPage = 'products.html';
$.subPagesDirectory = '';
$.page404 = 'views/pages/404.html';
$.mainContent = $('#ui-view');

//Main navigation
$.navigation = $('#navbar > .nav > li');

$.panelIconOpened = 'icon-arrow-up';
$.panelIconClosed = 'icon-arrow-down';

//Default colours
$.brandPrimary = '#20a8d8';
$.brandSuccess = '#4dbd74';
$.brandInfo = '#63c2de';
$.brandWarning = '#f8cb00';
$.brandDanger = '#f86c6b';

$.grayDark = '#2a2c36';
$.gray = '#55595c';
$.grayLight = '#818a91';
$.grayLighter = '#d1d4d7';
$.grayLightest = '#f8f9fa';

'use strict';

$('#main-container').removeClass('hide');

if ($.ajaxLoad) {
    var paceOptions = {
        elements: false,
        restartOnRequestAfter: false
    };

    var url = location.hash.replace(/^#/, '');

    if (url != '') {
        setUpUrl(url);
    } else {
        setUpUrl($.defaultPage);
    }

    $(document).on('click', '.nav a[href!="#"]', function (e) {
        if ($(this).parent().parent().hasClass('nav-tabs') || $(this).parent().parent().hasClass('nav-pills')) {
            e.preventDefault();
        } else if ($(this).attr('target') == '_top') {
            e.preventDefault();
            var target = $(e.currentTarget);
            window.location = (target.attr('href'));
        } else if ($(this).attr('target') == '_blank') {
            e.preventDefault();
            var target = $(e.currentTarget);
            window.open(target.attr('href'));
        } else {
            e.preventDefault();
            var target = $(e.currentTarget);
            setUpUrl(target.attr('href'));
        }
    });

    $(document).on('click', 'a[href="#"]', function (e) {
        e.preventDefault();
    });
}

function setUpUrl(url) {
    $.LoadingOverlaySetup({
        minSize         : "100px",
    });

    $.LoadingOverlay('show');

    setTimeout(function() {
        var a = $('#navbar .nav a[href="' + url.split('?')[0] + '"]');
        $('#navbar .nav li').removeClass('active');
        
        if (a.parent().parent().hasClass('dropdown-menu')) {
            a.parent().parent().parent().addClass('active');
        } else {
            a.parent('li').addClass('active');
        }

        loadPage(url);
    }, 1000);
    
}

function loadPage(url) {
    $.ajax({
        type: 'GET',
        url: $.subPagesDirectory + url,
        dataType: 'html',
        cache: false,
        async: false,
        beforeSend: function () {
            $.mainContent.css({ opacity: 0 });
        },
        success: function () {
            //Pace.restart();
            $('html, body').animate({ scrollTop: 0 }, 0);
            $.mainContent
                .load($.subPagesDirectory + url, null, function (responseText) {
                    window.location.hash = url;
                    $('.selectpicker').selectpicker('refresh');
                    $.LoadingOverlay('hide');
                })
                .delay(250)
                .animate({ opacity: 1 }, 0);
            
        },
        error: function () {
            window.location.href = $.page404;
        }

    });
}

/****
* MAIN NAVIGATION
*/

$(document).ready(function ($) {
    // Add class .active to current link - AJAX Mode off
    $.navigation.find('a').each(function () {

        var cUrl = String(window.location).split('?')[0];
        if (cUrl.substr(cUrl.length - 1) == '#') {
            cUrl = cUrl.slice(0, -1);
        }

        if ($($(this))[0].href == cUrl) {
            $(this).addClass('active');

            $(this).parents('ul').add(this).each(function () {
                $(this).parent().addClass('open');
            });
        }
    });

    // Dropdown Menu
    $.navigation.on('click', 'a', function (e) {

        if ($.ajaxLoad) {
            e.preventDefault();
        }

        if ($(this).hasClass('nav-dropdown-toggle')) {
            $(this).parent().toggleClass('open');
            resizeBroadcast();
        }
    });

    function resizeBroadcast() {

        var timesRun = 0;
        var interval = setInterval(function () {
            timesRun += 1;
            if (timesRun === 5) {
                clearInterval(interval);
            }
            window.dispatchEvent(new Event('resize'));
        }, 62.5);
    }

    $('.sidebar-close').click(function () {
        $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
    });

    /* ---------- Disable moving to top ---------- */
    $('a[href="#"][data-top!=true]').click(function (e) {
        e.preventDefault();
    });

});

$('#products-tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
	if (!$this.hasClass('panel-collapsed')) {
        var $form = $this.parents('.panel').find('.panel-heading').find('.search-form');
        setTimeout(function() {$form.hide()}, 300);

        $this.parents('.panel').find('.panel-body').slideUp();
		$this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
	} else {
        $this.parents('.panel').find('.panel-heading').find('.search-form').show();
		$this.parents('.panel').find('.panel-body').slideDown();
		$this.removeClass('panel-collapsed');
		$this.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
	}
});

$(document).ready(function(){
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }

        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 50) {
            $('#back-to-top').css('bottom', '70px');
        } else {
            $('#back-to-top').css('bottom', '20px');
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
       
    $('#back-to-top').tooltip('show');
});